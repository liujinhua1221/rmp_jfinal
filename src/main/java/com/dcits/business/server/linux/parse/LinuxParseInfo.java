package com.dcits.business.server.linux.parse;

import com.dcits.business.server.linux.LinuxMonitoringInfo;
import com.dcits.tool.StringUtils;

import java.util.Map;

public class LinuxParseInfo extends ParseInfo {
	
	protected LinuxParseInfo() {

	}


	/*12:09:03        IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
    12:09:04           lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
    12:09:04         eth0      1.01      0.00      0.06      0.00      0.00      0.00      0.00*/
	@Override
	public void parseNetworkInfo(String info, LinuxMonitoringInfo monitoringInfo) {

		Map<String, Object> map = monitoringInfo.getNetworkInfo();
		map.put(LinuxMonitoringInfo.NETWORK_RX, GET_INFO_FAILED_MSG);
		map.put(LinuxMonitoringInfo.NETWORK_TX, GET_INFO_FAILED_MSG);
		
		if (StringUtils.isNotEmpty(info)) {
		    int rxkBColumnSeq = 0;
		    int exkBColumnSeq = 0;
            String[] strs = info.split("\\n");
		    //获取  rxkB/s txkB/的列号
            String[] titleStr = strs[0].trim().split("\\s+");
            for (int i = 0; i < titleStr.length; i ++) {
                if ("rxkB/s".equalsIgnoreCase(titleStr[i])) {
                    rxkBColumnSeq = i;
                }
                if ("txkB/s".equalsIgnoreCase(titleStr[i])) {
                    exkBColumnSeq = i;
                }
            }

            //获取出入网带宽
			double rx = 0, tx = 0;

			String[] ss = null;
			for (int i = 1; i < strs.length; i ++) {
				ss = strs[i].trim().split("\\s+");
				rx += Double.parseDouble(ss[rxkBColumnSeq]);
				tx += Double.parseDouble(ss[exkBColumnSeq]);
			}

			map.put(LinuxMonitoringInfo.NETWORK_RX, String.format("%.2f", rx));
			map.put(LinuxMonitoringInfo.NETWORK_TX, String.format("%.2f", tx));
		}
	}

	@Override
	public void parseMountDevice(String info, LinuxMonitoringInfo monitoringInfo) {

		
	}

	@Override
	public void parseDeviceIOInfo(String info, LinuxMonitoringInfo monitoringInfo) {

		Map<String, Object> map = monitoringInfo.getIoInfo();
		map.put(LinuxMonitoringInfo.IO_READ, GET_INFO_FAILED_MSG);
		map.put(LinuxMonitoringInfo.IO_WRITE, GET_INFO_FAILED_MSG);

		if (StringUtils.isNotEmpty(info)) {
			double read = 0, write = 0;
			String[] strs = info.split("\\n");
			String[] ss = null;
			
			for (String s:strs) {
				if (StringUtils.isEmpty(s)) continue;
				ss = s.trim().split("\\s+");
				read += Double.parseDouble(ss[0]);
				write += Double.parseDouble(ss[1]);				
			}			
			map.put(LinuxMonitoringInfo.IO_READ, String.format("%.2f", read));
			map.put(LinuxMonitoringInfo.IO_WRITE, String.format("%.2f", write));			
		}
	}
}
