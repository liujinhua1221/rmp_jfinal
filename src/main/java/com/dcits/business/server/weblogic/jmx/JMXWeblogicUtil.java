package com.dcits.business.server.weblogic.jmx;

import com.dcits.business.server.weblogic.WeblogicMonitoringInfo;
import com.dcits.business.server.weblogic.WeblogicServer;
import com.dcits.tool.RmpUtil;
import weblogic.health.HealthState;

import javax.management.*;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.*;

public class JMXWeblogicUtil {
	
	private static final String RUNTIMESERVICEMBEAN = "com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean";
	
	private static final String PROTOCOL = "t3";
	
	private static final String JINDIROOT = "/jndi/";
	
	private static final String MSERVER = "weblogic.management.mbeanservers.runtime";	
	
	/**
	 * 初始化jmx连接
	 * @param weblogic
	 * @throws MalformedObjectNameException
	 * @throws IOException
	 */
	public static void getConnection(WeblogicServer weblogic) throws Exception {
		
		Integer portInteger = weblogic.getPort();
		JMXServiceURL serviceURL = new JMXServiceURL(PROTOCOL, weblogic.getHost(), portInteger, JINDIROOT + MSERVER);
		
		Hashtable<String, String> h = new Hashtable<String, String>();
		h.put(Context.SECURITY_PRINCIPAL, weblogic.getUsername());
	    h.put(Context.SECURITY_CREDENTIALS, weblogic.getPassword());
	    h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");

	    weblogic.setConnection(JMXConnectorFactory.connect(serviceURL, h).getMBeanServerConnection());
	    
	    ObjectName runtimeService = new ObjectName(RUNTIMESERVICEMBEAN);
	    
	    //运行状态
	    ObjectName serverRuntime =  RmpUtil.getAttribute(runtimeService, "ServerRuntime", weblogic.getConnection());	    
	    weblogic.setServerRuntime(serverRuntime);
	    //JVM状态
	    ObjectName jvmRuntime =  RmpUtil.getAttribute(serverRuntime, "JVMRuntime", weblogic.getConnection());
	    weblogic.setJvmRuntime(jvmRuntime);
	    
	    //线程状态
	    ObjectName threadPoolRuntime =  RmpUtil.getAttribute(serverRuntime, "ThreadPoolRuntime", weblogic.getConnection());
	    weblogic.setThreadPoolRuntime(threadPoolRuntime);
	    
	    //jdbc状态
	    ObjectName jdbcServiceRuntime = RmpUtil.getAttribute(serverRuntime, "JDBCServiceRuntime", weblogic.getConnection());	    
	    weblogic.setJdbcRuntime(jdbcServiceRuntime);    
	}
	
	/**
	 * 获取weblogic信息
	 * 前台调用一次weblogic/getInfo?weblogicId=?就执行一次该方法
	 * 
	 * @param weblogic
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws MBeanException
	 * @throws ReflectionException
	 * @throws IOException
	 */
	public static void getWeblogicInfo(WeblogicServer weblogic) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException, Exception {
		if (weblogic.getConnection() == null) {
			throw new Exception("连接不可用");
		}
		//getServerRuntime 获取运行信息
		WeblogicMonitoringInfo info = (WeblogicMonitoringInfo) weblogic.getInfo();
		MBeanServerConnection conn = weblogic.getConnection();
		
		//运行状态情况
		ObjectName serverRuntime = weblogic.getServerRuntime();
		//jvm情况
	    ObjectName JVMRuntime = weblogic.getJvmRuntime();
		
	    //线程队列情况
	    ObjectName threadPoolRuntime = weblogic.getThreadPoolRuntime();
	    
	    //jdbc数据源
	    ObjectName jdbcRuntime = weblogic.getJdbcRuntime();
		
		//是否首次获取或者是否重新获取新的固定信息？
		if (weblogic.getIfFirstGetInfo()) {
			//server的name
			String serverName =  RmpUtil.getAttribute(serverRuntime, "Name", conn);
			
			//server的激活时间
			Long activationTime =  RmpUtil.getAttribute(serverRuntime, "ActivationTime",  conn);
		    Date date = new Date(activationTime);
		    String time =  RmpUtil.formatDate(date, "yyyy/MM/dd HH:mm:ss");
		    
		    //运行状态
		    String state =  RmpUtil.getAttribute(serverRuntime, "State", conn);
		    
		    //健康状态
		    HealthState healthState = (HealthState) conn.getAttribute(serverRuntime, "HealthState");
		    String health = healthState.getState() == 0 ? "Health" : "Not Health";
		    
		    //最大堆内存大小
		    Long heapSizeMax =  RmpUtil.getAttribute(JVMRuntime, "HeapSizeMax", conn);
		    String maxJvm =  RmpUtil.byteToMB(heapSizeMax);
		    
		    info.setServerName(serverName);
		    info.setHealth(health);
		    info.setStatus(state);
		    info.setStartTime(time);
		    info.setMaxJvm(maxJvm);
		    
		    weblogic.setIfFirstGetInfo(false);
		}
		
	    //空闲内存百分比
	    Integer heapFreePercent =  RmpUtil.getAttribute(JVMRuntime, "HeapFreePercent", conn);
	    String freePercent = String.valueOf(heapFreePercent);
	    
	    //当前已使用的堆的总空间
	    Long heapSizeCurrent =  RmpUtil.getAttribute(JVMRuntime, "HeapSizeCurrent", conn);
	    String currentSize =  RmpUtil.byteToMB(heapSizeCurrent);
	    
	    // 当前堆 空闲 HeapFreeCurrent
	    Long heapFreeCurrent =  RmpUtil.getAttribute(JVMRuntime, "HeapFreeCurrent", conn);
	    String freeSize =  RmpUtil.byteToMB(heapFreeCurrent);
	    
	    // 执行线程总数 
        Integer executeThreadTotalCount =  RmpUtil.getAttribute(threadPoolRuntime, "ExecuteThreadTotalCount", conn);
        String maxThreadCount = String.valueOf(executeThreadTotalCount);
	        	    
	    //空闲线程数
	    Integer executeThreadCurrentIdleCount =  RmpUtil.getAttribute(
	    		threadPoolRuntime, "ExecuteThreadIdleCount", conn);
	    String idleCount = String.valueOf(executeThreadCurrentIdleCount);
	    
	    //暂挂请求数
	    Integer pendingRequestCurrentCount = RmpUtil.getAttribute(
	    		threadPoolRuntime, "PendingUserRequestCount", conn);
	    String pendingCount = String.valueOf(pendingRequestCurrentCount);
	         
	    //独占线程数
	    Integer hoggingThreadCount = RmpUtil.getAttribute(threadPoolRuntime, "HoggingThreadCount", conn);
	    String hoggingCount = String.valueOf(hoggingThreadCount);
	    
	    //吞吐量
	    Double throughput  = RmpUtil.getAttribute(threadPoolRuntime, "Throughput", conn);	    
	    String throughputs = String.format("%.2f", throughput);
	    
	    ObjectName[] jdbcDataSourceRuntimeMBeans = RmpUtil.getAttribute(jdbcRuntime, "JDBCDataSourceRuntimeMBeans", weblogic.getConnection());
	    
	    if (jdbcDataSourceRuntimeMBeans.length > 0) {
	    	
	    	for (ObjectName o:jdbcDataSourceRuntimeMBeans) {
	    		Map<String, String> jdbcInfoMap = new HashMap<String, String>();
	    		
	    		 //jdbc数据源状态
			    String jdbcState = RmpUtil.getAttribute(o, "State", conn);
			    
			    //jdbc当前活动连接数
			    Integer activeConnectionCount = RmpUtil.getAttribute(o, "ActiveConnectionsCurrentCount", conn);
			    
			    //jdbc当前等待连接数
			    Integer waittingConnectionCount = RmpUtil.getAttribute(o, "WaitingForConnectionCurrentCount", conn);
			    
			    //jdbc当前可用连接数
			    Integer availableNum = RmpUtil.getAttribute(o, "NumAvailable", conn);
			    
			    //历史最大活动连接数
			    Integer activeConnectionsHighCount = RmpUtil.getAttribute(o, "ActiveConnectionsHighCount", conn);
			    
			    jdbcInfoMap.put(WeblogicMonitoringInfo.JDBC_DATA_SOURCE_STATE, jdbcState);
			    jdbcInfoMap.put(WeblogicMonitoringInfo.JDBC_ACTIVE_CONNECTIONS_CURRENT_COUNT, String.valueOf(activeConnectionCount));
			    jdbcInfoMap.put(WeblogicMonitoringInfo.JDBC_WAITING_FOR_CONNECTION_CURRENT_COUNT, String.valueOf(waittingConnectionCount));
			    jdbcInfoMap.put(WeblogicMonitoringInfo.JDBC_CURRENT_NUM_AVAILABLE, String.valueOf(availableNum));
			    jdbcInfoMap.put(WeblogicMonitoringInfo.JDBC_ACTIVE_CONNECTIONS_HIGH_COUNT, String.valueOf(activeConnectionsHighCount));
			    
			    info.getJdbcInfo().put((String)RmpUtil.getAttribute(o, "Name", conn), jdbcInfoMap);	    		
	    	}    	
	    }
	    	    
	    info.getJvmInfo().put(WeblogicMonitoringInfo.JVM_CURRENT_USE_SIZE, currentSize);
	    info.getJvmInfo().put(WeblogicMonitoringInfo.JVM_FREE_PERCENT, freePercent);
	    info.getJvmInfo().put(WeblogicMonitoringInfo.JVM_FREE_SIZE, freeSize);
	    
	    info.getQueueInfo().put(WeblogicMonitoringInfo.THREAD_IDLE_COUNT, idleCount);
	    info.getQueueInfo().put(WeblogicMonitoringInfo.THREAD_REQUEST_PENDING_COUNT, pendingCount);
	    info.getQueueInfo().put(WeblogicMonitoringInfo.THREAD_TOTAL_COUNT, maxThreadCount);
	    info.getQueueInfo().put(WeblogicMonitoringInfo.THREAD_HOGGING_COUNT, hoggingCount);
	    info.getQueueInfo().put(WeblogicMonitoringInfo.THREAD_THROUGHPUT, throughputs);
	    	    	    
	    info.setTime(new Date());	    
	}


	/**
	 * 连接weblogic时设置超时时间
	 * @author xuwangcheng
	 * @date 2020/1/9 1:27
	 * @param url url
	 * @param h h
	 * @param timeout timeout
	 * @param unit unit
	 * @return {@link JMXConnector}
	 */
    private static JMXConnector connectWithTimeout(final JMXServiceURL url,final Hashtable<String, String> h, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<JMXConnector> future = executor.submit(new Callable<JMXConnector>() {
            @Override
            public JMXConnector call() throws IOException {
                return JMXConnectorFactory.connect(url, h);
            }
        });
        return future.get(timeout, unit);
    }

    public static void checkConnection (WeblogicServer weblogic) throws Exception {
        try {
            RmpUtil.getAttribute(
                    weblogic.getThreadPoolRuntime(), "ExecuteThreadIdleCount", weblogic.getConnection());
        } catch (Exception e) {
            if (e instanceof NullPointerException) {
                try {
                    getConnection(weblogic);
                } catch (Exception e1) {
                    throw new Exception("无法连接到服务器");
                }
            }
        }
    }
}
